package me.dewgs.particlemod;

import net.minecraft.command.CommandException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.client.Minecraft;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.CommandBase;
import net.minecraft.util.text.TextComponentString;

public class ParticleModCommand extends CommandBase
{
    private ParticleMod mod;
    
    public ParticleModCommand(final ParticleMod mod) {
        this.mod = mod;
    }
    
    public String getCommandName() {
        return "particlemod";
    }
    
    public String getCommandUsage(final ICommandSender sender) {
        return "/particlemod <multiplier>";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length < 1) {
            Minecraft.getMinecraft().thePlayer.addChatMessage(new TextComponentString("Current particle multiplier: " + this.mod.multiplier));
            Minecraft.getMinecraft().thePlayer.addChatMessage(new TextComponentString(""));
            Minecraft.getMinecraft().thePlayer.addChatMessage(new TextComponentString("Change it by doing " + this.getCommandUsage(sender)));
            return;
        }
        if (!this.tryParse(args[0])) {
            Minecraft.getMinecraft().thePlayer.addChatMessage(new TextComponentString("Usage: " + this.getCommandUsage(sender)));
            return;
        }
        final Integer multiplier = Integer.parseInt(args[0]);
        if (multiplier < 1) {
            Minecraft.getMinecraft().thePlayer.addChatMessage(new TextComponentString("Usage: " + this.getCommandUsage(sender)));
            return;
        }
        this.mod.multiplier = multiplier;
        this.mod.saveConfig();
        Minecraft.getMinecraft().thePlayer.addChatMessage(new TextComponentString("Particle multiplier set to " + multiplier + "!"));
    }

    public int getRequiredPermissionLevel() {
        return 0;
    }
    
    public boolean func_71519_b(final ICommandSender sender) {
        return true;
    }
    
    private boolean tryParse(final String parse) {
        try {
            Integer.parseInt(parse);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }
}
