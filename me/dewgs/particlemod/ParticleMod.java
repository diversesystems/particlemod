package me.dewgs.particlemod;

import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.Entity;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.potion.Potion;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraft.command.ICommand;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "particlemod", version = "1.02")
public class ParticleMod
{
    public static final String MODID = "particlemod";
    public static final String VERSION = "1.02";
    public int multiplier;
    
    public ParticleMod() {
        this.multiplier = 1;
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register((Object)this);
        ClientCommandHandler.instance.registerCommand((ICommand)new ParticleModCommand(this));
        this.loadConfig();
        System.out.println("Successfully initialized ParticleMod 1.02 by dewgs");
    }
    
    @SubscribeEvent
    public void onAttack(final AttackEntityEvent event) {
        final Entity entity = event.getTarget();
        if (!(entity instanceof EntityLivingBase)) {
            return;
        }
        final EntityPlayer player = (EntityPlayer)Minecraft.getMinecraft().thePlayer;
        final boolean critical = player.fallDistance > 0.0f && !player.onGround && !player.isOnLadder() && !player.isInWater() && !player.isPotionActive(Potion.getPotionFromResourceLocation("blindness")) && !player.isRiding();
        final float enchantment = EnchantmentHelper.getModifierForCreature(player.getHeldItemMainhand(), ((EntityLivingBase)entity).getCreatureAttribute());
        for (int i = 1; i < this.multiplier; ++i) {
            if (critical) {
                Minecraft.getMinecraft().thePlayer.onCriticalHit(entity);
            }
            if (enchantment > 0.0f) {
                Minecraft.getMinecraft().thePlayer.onEnchantmentCritical(entity);
            }
        }
    }
    
    public void saveConfig() {
        try {
            final File file = new File(Minecraft.getMinecraft().mcDataDir + File.separator + "ParticleMod", "value.cfg");
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            final FileWriter writer = new FileWriter(file, false);
            writer.write(this.multiplier + "");
            writer.close();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    private void loadConfig() {
        try {
            final File file = new File(Minecraft.getMinecraft().mcDataDir + File.separator + "ParticleMod", "value.cfg");
            if (!file.exists()) {
                return;
            }
            final BufferedReader reader = new BufferedReader(new FileReader(file));
            final String line = reader.readLine();
            if (line != null) {
                this.multiplier = Integer.parseInt(line);
            }
            reader.close();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
